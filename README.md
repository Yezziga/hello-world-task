# hello-world-task

Screenshot of the .java file being compiled to .class:

![](CompileProgram.PNG)

Screenshot of creating a .jar file of the .class:

![](CreateJarFile.PNG)

Screenshot of the execution of the .jar file:

![](ExecuteJarFile.PNG)
